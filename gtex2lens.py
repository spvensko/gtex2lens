#!/usr/bin/env python

import argparse
import gzip
import numpy as np
from pprint import pprint

def get_args():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest='command')
   
    parser_summarize_tx = subparsers.add_parser('summarize-transcript-expression')
    parser_summarize_tx.add_argument('-t', '--tx-file', required=True) 
    parser_summarize_tx.add_argument('-m', '--sample-map', required=True)
    parser_summarize_tx.add_argument('-o', '--output', required=True)
    parser_summarize_tx.add_argument('-w', '--wide-output', required=True)
    
    return parser.parse_args() 


def summarize_transcript_expression(args):
    """
    """


    sample_map = parse_tissue_codes(args.sample_map)

    samp_col_idx_map = {}
    
    summary_stats = {}

    tissues = []
    
    with open(args.output, 'w') as ofo:
        ofo.write("identifier\ttissue\tmean\tmedian\tmax\tiqr\n")
        with open(args.tx_file, 'rt') as fo:
            for line_idx, line in enumerate(fo.readlines()):
                if line_idx == 2:
                    line = line.rstrip().split('\t')
                    for entry_idx, entry in enumerate(line):
                        samp_col_idx_map[entry_idx] = entry
                elif line_idx > 2:
                    line = line.rstrip().split('\t')
                    identifier = ''
                    bufr = {}
                    for val_idx, val in enumerate(line):
                        if val_idx == 0:
                            identifier = val
                            summary_stats[identifier] = {}
                        elif val_idx > 1:
                            samp = samp_col_idx_map[val_idx]
                            tissue_type = sample_map[samp]
                            summary_stats[identifier][tissue_type] = {}
                            if tissue_type in bufr.keys():
                                bufr[tissue_type].append(float(val))
                            else:
                                bufr[tissue_type] = [float(val)]
                            if tissue_type not in tissues:
                                tissues.append(tissue_type)
                    for k, v in bufr.items():
                        mean = np.mean(v)
                        median = np.median(v)
                        max = np.max(v)
                        q75, q25 = np.percentile(v, [75 ,25])
                        iqr = q75 - q25
                        summary_stats[identifier][tissue_type]['mean'] = mean
                        summary_stats[identifier][tissue_type]['median'] = median
                        summary_stats[identifier][tissue_type]['max'] = max
                        summary_stats[identifier][tissue_type]['iqr'] = iqr
                        ofo.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, k, mean, median, max, iqr))
   
    with open(args.wide_output, 'w') as wofo:
        wofo.write("identifier\t")
        for tissue in tissues:
            wofo.write("{0}_tpm_mean\t{0}_tpm_median\t{0}_tpm_max\t{0}_tpm_iqr\n")
#        with open(args.tx_file, 'rt') as fo:
#            for line_idx, line in enumerate(fo.readlines()):
#                if line_idx == 2:
#                    line = line.rstrip().split('\t')
#                    for entry_idx, entry in enumerate(line):
#                        samp_col_idx_map[entry_idx] = entry
#                elif line_idx > 2:
#                    line = line.rstrip().split('\t')
#                    identifier = ''
#                    bufr = {}
#                    for val_idx, val in enumerate(line):
#                        if val_idx == 0:
#                            identifier = val
#                        elif val_idx > 1:
#                            samp = samp_col_idx_map[val_idx]
#                            tissue_type = sample_map[samp]
#                            if tissue_type in bufr.keys():
#                                bufr[tissue_type].append(float(val))
#                            else:
#                                bufr[tissue_type] = [float(val)]
#                    for k, v in bufr.items():
#                        mean = np.mean(v)
#                        median = np.median(v)
#                        max = np.max(v)
#                        q75, q25 = np.percentile(v, [75 ,25])
#                        iqr = q75 - q25
#                        ofo.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, k, mean, median, max, iqr))




def parse_tissue_codes(sample_map):

    eventual_sample_map = {}

    with open(sample_map) as fo:
        for line_idx, line in enumerate(fo.readlines()):
            if line_idx > 0:
                line = line.split('\t')
                sample = line[0] 
                tissue = line[5]
                eventual_sample_map[sample] = tissue
    return eventual_sample_map


    


def main():
   args = get_args()
   
   print(args)

   if args.command == 'summarize-transcript-expression':
       summarize_transcript_expression(args)

if __name__ == '__main__':
    main()

