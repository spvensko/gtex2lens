#!/usr/bin/env python                                                                               
                                                                                                    
import argparse                                                                                     
import gzip                                                                                         
import numpy as np                                                                                  
from pprint import pprint                                                                           
                                                                                                    
def get_args():                                                                                     
    parser = argparse.ArgumentParser()                                                              
                                                                                                    
    subparsers = parser.add_subparsers(dest='command')                                              
                                                                                                    
    parser_summarize_tx = subparsers.add_parser('summarize-transcript-expression')                  
    parser_summarize_tx.add_argument('-t', '--gtex-summ', required=True)                              
    parser_summarize_tx.add_argument('-o', '--output', required=True)                               
                                                                                                    
    return parser.parse_args() 



def main():
    """
    """
    args = get_args()

    tissues = ['Colon', 'Skin', 'Liver']

    summ_stats = {}

    with open(args.gtex_summ) as fo:
        for line_idx, line in enumerate(fo.readlines()):
            if line_idx > 0:
                line = line.strip().split('\t')
                if line[0] not in summ_stats.keys():
                    summ_stats[line[0]] = {}
#                if line[1] not in tissues:
#                    tissues.append(line[1])
                summ_stats[line[0]][line[1]] = {}
                summ_stats[line[0]][line[1]]['mean'] = line[2] 
                summ_stats[line[0]][line[1]]['median'] = line[3] 
                summ_stats[line[0]][line[1]]['max'] = line[4] 
                summ_stats[line[0]][line[1]]['iqr'] = line[5] 

    with open(args.output, 'w') as ofo:
       ofo.write("identifier\t")
       for tissue in tissues:
           for metric in ['mean', 'median', 'max', 'iqr']:
               ofo.write("{}_{}\t".format(tissue.replace(' ', '_'), metric))
       ofo.write("\n")
       for ident, tissues in summ_stats.items():
           ofo.write("{}\t".format(ident))
           for tissue in tissues:
               for metric in ['mean', 'median', 'max', 'iqr']:
                   ofo.write("{}\t".format(summ_stats[ident][tissue][metric]))
           ofo.write("\n")
        

if __name__=='__main__':
    main()
